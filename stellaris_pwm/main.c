/*
* Copyright (c) 2012, Mauro Scomparin
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of Mauro Scomparin nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY Mauro Scomparin ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL Mauro Scomparin BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* File:			main.c.
* Author:		Mauro Scomparin <http://scompoprojects.worpress.com>.
* Version:		1.0.0.
* Description:	Main sample file.
*/

#include "inc/hw_ints.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "driverlib/pin_map.h"
#include <stdint.h>

// setup the duty cicle
void LedBlueDuty(unsigned long val)
{
	// setup the duty cicle
	TimerMatchSet(TIMER1_BASE, TIMER_A, val);
}

// setup the duty cicle
void LedRedDuty(unsigned long val)
{
	// setup the duty cicle
	TimerMatchSet(TIMER0_BASE, TIMER_B, val);
}

// setup the duty cicle
void LedGreenDuty(unsigned long val)
{
	// setup the duty cicle
	TimerMatchSet(TIMER1_BASE, TIMER_B, val);
}


unsigned long RgbInit(unsigned long frequency)
{
	unsigned long  TimerTicks;

	// enable peripherals
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);

	// config pins !
	TimerTicks = (SysCtlClockGet()/frequency);

	// Configure GPIO
	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
	GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3,0x00);
	GPIOPinConfigure(GPIO_PF1_T0CCP1); // R
	GPIOPinConfigure(GPIO_PF2_T1CCP0); // B
	GPIOPinConfigure(GPIO_PF3_T1CCP1); // G
	GPIOPinTypeTimer(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

	// Red led !
	TimerConfigure(TIMER0_BASE, TIMER_CFG_SPLIT_PAIR|TIMER_CFG_B_PWM);
	TimerLoadSet(TIMER0_BASE, TIMER_B, TimerTicks);
	TimerMatchSet(TIMER0_BASE, TIMER_B, TimerTicks-1);
	TimerEnable(TIMER0_BASE, TIMER_B);

	// Blue led !
	// TimerConfigure pwm A and pwm B enabled !
	// A is for T1CCP0 and B is for T1CCP1
	TimerConfigure(TIMER1_BASE, TIMER_CFG_SPLIT_PAIR|TIMER_CFG_A_PWM|TIMER_CFG_B_PWM);
	TimerLoadSet(TIMER1_BASE, TIMER_A, TimerTicks);
	TimerMatchSet(TIMER1_BASE, TIMER_A, TimerTicks-1);
	TimerEnable(TIMER1_BASE, TIMER_A);

	// Green led !
	TimerLoadSet(TIMER1_BASE, TIMER_B, TimerTicks);
	TimerMatchSet(TIMER1_BASE, TIMER_B, TimerTicks-1);
	TimerEnable(TIMER1_BASE, TIMER_B);

	return TimerTicks-1;
}

// Setup led RGB (0-255,0-255,0-255, maxticks)
void Rgb( unsigned long red, unsigned long green, unsigned long blue, unsigned long maxticks)
{
	LedRedDuty(((255-red)*maxticks)/255);
	LedGreenDuty(((255-green)*maxticks)/255);
	LedBlueDuty(((255-blue)*maxticks)/255);
}

// main function.
int main(void) {
	unsigned long ulPeriod, ulfreq;

	// 40 Mhz system clock
	SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|
		SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
    GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, 0);
	
	ulfreq = 400000;
	ulPeriod = RgbInit(ulfreq);
	
   	uint8_t r=0, g=0, b=0;
	uint8_t state=0;

    while(1)
    {

    	// RGB combination !!!
    	switch (state)
    	{
    		case 0:
    			r=0;g=0;b=0;
    			state++;
    			break;
    		case 1:
    			if(r==255) state++;
    			r++;
    			break;
		    case 2:
				if(g==255) state++;
				g++;
				break;
			case 3:
				if(b==255) state++;
				b++;
				break;
			case 4:
				if(r==255) state++;
				r++; g++;
				break;
			case 5:
				if(r==255) state++;
				r++; b++;
				break;
			case 6:
				if(g==255) state++;
				g++;b++;
				break;
			case 7:
				if(r==255) state=0;
				r++;g++;b++;
				break;
	    }

	    // setup the colour of the rgb led 
    	Rgb( r, g, b, ulPeriod);
    	SysCtlDelay(50000);
    }
}
